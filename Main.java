import com.zuitt.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact johnDoe = new Contact("John Doe", "+639152468596", "+639228547963", "My home in Quezon City", "My office in Makati City");
        Contact janeDoe = new Contact("Jane Doe", "+639162148900", "+639234252342", "My home in Caloocan City", "My office in Pasay City");

        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);

        if (phonebook.isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("---------------------------\n");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber1());
                System.out.println(contact.getContactNumber2() + "\n");
                System.out.println("---------------------------\n");
                System.out.println(contact.getName() + " has the following registered Addresses:");
                System.out.println(contact.getAddress1());
                System.out.println(contact.getAddress2() + "\n");
                System.out.println("=====================================\n");
            }
        }
    }
}
